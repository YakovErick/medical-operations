<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'department_id' => $faker->numberBetween(1, 6),
        'remember_token' => Str::random(10),
    ];
});

$factory->define(\App\Models\Checkin::class, function (Faker $faker) {
    return [
        'patient_id' => $faker->numberBetween(1, 300),
        'department_id' => $faker->numberBetween(1, 6),
        'time_in' => $faker->dateTime,
        'time_out' => $faker->dateTime,
        'checked' => $faker->randomKey([0, 1]),
        'checked_in_by' => $faker->numberBetween(1, 41)
    ];
});

$factory->define(\App\Models\Patient::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'sex' => $faker->randomElement(['male', 'female']),
        'blood_type' => $faker->randomElement([
            'O negative',
            'O positive',
            'A positive',
            'A negative',
            'B negative',
            'B positive',
            'AB negative',
            'AB positive'
        ]),
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'emergency_contact' => $faker->phoneNumber
    ];
});

$factory->define(\App\Models\Treatment::class, function (Faker $faker) {
    return [
        'patient_id' => $faker->numberBetween(1, 300),
        'user_id' => $faker->numberBetween(1, 41),
        'diagnosis' => $faker->text,
        'illness' => $faker->safeColorName,
        'recommendation' => $faker->text,
        'date' => $faker->dateTime
    ];
});

$factory->define(\App\Models\Referral::class, function (Faker $faker) {
    return [
        'patient_id' => $faker->numberBetween(1, 300),
        'department_from' => $faker->numberBetween(1, 6),
        'department_to' => $faker->numberBetween(1, 6),
        'referred_by' => $faker->numberBetween(1, 41),
        'date' => $faker->dateTime
    ];
});

$factory->define(\App\Models\Department::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->text,
        'can_refer' => $faker->randomKey([0,1])
    ];
});
