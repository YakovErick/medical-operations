<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(\App\User::class, 40)->create();

        $departments = $this->getDepartments();

        foreach($departments as $department) {
            \App\Models\Department::create($department);
        }

        factory(\App\Models\Checkin::class, 300)->create();

        factory(\App\Models\Patient::class, 300)->create();

        factory(\App\Models\Referral::class, 50)->create();

        factory(\App\Models\Treatment::class, 5000)->create();
    }

    public function getDepartments()
    {
        return [
            [
                'name' => 'Nursing',
                'slug' => str_slug('Nursing'),
                'can_refer' => false
            ],

            [
                'name' => 'Laboratory',
                'slug' => str_slug('Laboratory'),
                'can_refer' => true
            ],

            [
                'name' => 'Radiology',
                'slug' => str_slug('Radiology'),
                'can_refer' => true
            ],

            [
                'name' => 'Treatment',
                'slug' => str_slug('Treatment'),
                'can_refer' => true
            ],

            [
                'name' => 'Optical',
                'slug' => str_slug('Optical'),
                'can_refer' => true
            ],

            [
                'name' => 'Reception',
                'slug' => str_slug('Reception'),
                'can_refer' => false
            ]
        ];
    }
}
