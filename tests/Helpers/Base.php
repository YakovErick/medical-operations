<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/5/19
 * Time: 11:04 AM
 */

namespace Tests\Helpers;


use App\Models\Department;
use App\User;

trait Base
{
    public function setupDepartments()
    {
        $departments = $this->getDepartments();

        foreach($departments as $department) {
            factory(Department::class)->create($department);
        }
    }

    public function getDepartments()
    {
        return [
            [
                'name' => 'Nursing',
                'slug' => str_slug('Nursing'),
                'can_refer' => false
            ],

            [
                'name' => 'Laboratory',
                'slug' => str_slug('Laboratory'),
                'can_refer' => true
            ],

            [
                'name' => 'Radiology',
                'slug' => str_slug('Radiology'),
                'can_refer' => true
            ],

            [
                'name' => 'Treatment',
                'slug' => str_slug('Treatment'),
                'can_refer' => true
            ],

            [
                'name' => 'Optical',
                'slug' => str_slug('Optical'),
                'can_refer' => true
            ],

            [
                'name' => 'Reception',
                'slug' => str_slug('Reception'),
                'can_refer' => false
            ]
        ];
    }

    public function setupPractitioners()
    {
        return factory(User::class, 40)->create();
    }
}
