<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/5/19
 * Time: 10:59 AM
 */

namespace Tests\Feature;


use App\Models\Patient;
use Tests\Helpers\Base;
use Tests\TestCase;
use Faker\Factory as Faker;

class PatientCreateTest extends TestCase
{
    use Base;

    protected $faker;

    public function initialize()
    {
        $this->faker = Faker::create();

        $this->setupDepartments();

        $this->setupPractitioners();
    }

    public function testBasicTest()
    {
        $data = [
            'name' => 'name',
            'sex' => 'Male',
            'blood_type' => 'AB',
            'phone' => '071oooooo',
            'email' => 'test@gmail.com',
            'emergency_contact' => '072000000'
        ];

        $this->post('/patients', $data)
            ->isSuccessful();

        $table = (new Patient())->getTable();

//        $this->assertDatabaseHas($table, [ 'name' => $data['name']]);
    }
}
