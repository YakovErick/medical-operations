<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('/user/login', [
    'as' => 'user_login',
    'uses' => 'Auth\AuthController@login'
]);

Route::post('/user/register', [
    'as' => 'user_register',
    'uses' => 'Auth\AuthController@register'
]);

Route::get('/home', 'HomeController@index')->name('home');


Route::post('/patients', 'PatientController@store');
