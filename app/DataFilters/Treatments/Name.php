<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 6:05 AM
 */

namespace App\DataFilters\Treatments;


use App\DataFilters\Filters;
use Illuminate\Database\Eloquent\Builder;

class Name implements Filters
{
    public static function apply(Builder $model, $column, $value)
    {
        if (!$value || $value == '') {
            return $model;
        }

        return $model->whereHas('patient', function ($patient) use ($value) {
            $patient->where('name', 'LIKE', "%{$value}%");
        });
    }
}
