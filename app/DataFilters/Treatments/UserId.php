<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 6:04 AM
 */

namespace App\DataFilters\Treatments;


use App\DataFilters\Filters;
use Illuminate\Database\Eloquent\Builder;

class UserId implements Filters
{
    public static function apply(Builder $model, $column, $value)
    {
        return $model->where($column, $value);
    }
}
