<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 6:05 AM
 */

namespace App\DataFilters\Treatments;


use App\DataFilters\Filters;
use Illuminate\Database\Eloquent\Builder;

class DepartmentId implements Filters
{
    public static function apply(Builder $model, $column, $value)
    {
        if(!$value) {
            return $model;
        }

        return $model->whereHas('user', function ($user) use ($value) {
            $user->where('department_id', $value);
        });
    }
}
