<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 6:03 AM
 */

namespace App\DataFilters;


use Illuminate\Database\Eloquent\Builder;

interface Filters
{
    public static function apply(Builder $model, $column, $value);
}
