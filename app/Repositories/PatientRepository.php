<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 5:57 AM
 */

namespace App\Repositories;


use App\Models\Patient;
use App\Traits\Paginate;
use App\Transformers\PatientsTransformer;
use Illuminate\Http\Response;

class PatientRepository
{
    use Paginate;

    public function fetchPatients()
    {
        return $this->sortFilterPaginate(

            (new Patient())->newQuery(),

            [],

            function ($patient) {
                return app(PatientsTransformer::class)->transform($patient);
            },

            function ($model) {
                return $model;
            }
        );
    }

    public function save($data)
    {
        $patient = Patient::create($data);

        return $patient;
    }

    public function show($id)
    {
        return Patient::findOrFail($id);
    }

    public function update($data, $id)
    {
        $patient = Patient::findOrFail($id);

        if ($patient->isClean()) {
            return response()->json([
                'message' => 'At least one value must change'
            ], Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $patient->update($data);

        return $patient;
    }

    public function delete($id)
    {
        $patient = Patient::findOrFail($id);

        $patient->delete();

        return $patient;
    }
}
