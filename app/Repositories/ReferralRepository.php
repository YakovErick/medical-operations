<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/5/19
 * Time: 10:43 AM
 */

namespace App\Repositories;


use App\Models\Patient;
use App\Models\Referral;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ReferralRepository
{
    public function save($data)
    {
        $patient = Patient::findOrFail($data['patient_id']);

        $data['referred_by'] = Auth::user();

        $checkin = $patient->checkins->latest();

        $data['department_id'] = $checkin->department->id;

        $referral = Referral::create($data);

        $referral->save();

        return $checkin;
    }

    public function show($id)
    {
        return Referral::findOrFail($id);
    }

    public function update($data, $id)
    {
        $referral = Referral::findOrFail($id);

        if ($referral->isClean()) {
            return response()->json([
                'message' => 'At least one value must change'
            ], Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $referral->update($data);

        return $referral;
    }

    public function delete($id)
    {
        $referral = Referral::findOrFail($id);

        $referral->delete();

        return $referral;
    }
}
