<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/5/19
 * Time: 10:36 AM
 */

namespace App\Repositories;


use App\Models\Checkin;
use App\Models\Patient;
use Illuminate\Http\Response;

class CheckinRepository
{
    public function save($data)
    {
        $patient = Patient::findOrFail($data['patient_id']);

        unset($data['patient_id']);

        $checkin = Checkin::create($data);

        $checkin = $checkin->save();

        $checkin->patient()->associate($patient);

        return $checkin;
    }

    public function show($id)
    {
        return Checkin::findOrFail($id);
    }

    public function update($data, $id)
    {
        $checkin = Checkin::findOrFail($id);

        if ($checkin->isClean()) {
            return response()->json([
                'message' => 'At least one value must change'
            ], Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $checkin->update($data);

        return $checkin;
    }

    public function delete($id)
    {
        $checkin = Checkin::findOrFail($id);

        $checkin->delete();

        return $checkin;
    }
}
