<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 5:57 AM
 */

namespace App\Repositories;


use App\Models\Patient;
use App\Models\Treatment;
use App\Traits\Paginate;
use App\Transformers\TreatmentsTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;

class TreatmentRepository
{
    use Paginate;

    public function fetchTreatments($filters)
    {
        return $this->sortFilterPaginate(

            (new Treatment())->newQuery(),

            [],

            function ($patient) {
                return app(TreatmentsTransformer::class)->transform($patient);
            },

            function ($model) use($filters) {
                return $this->applyFiltersToQuery($filters, $model);
            }
        );
    }

    private static function applyFiltersToQuery($filters, Builder $model)
    {
        foreach ($filters as $filterName => $value) {

            $decorator = 'App\DataFilters\Treatments' . '\\' .
                str_replace(' ', '',
                    ucwords(str_replace('_', ' ', $filterName)));

            if (class_exists($decorator)) {
                $model = $decorator::apply($model, $filterName, $value);
            }
        }

        return $model;
    }

    public function save($data)
    {
        $patient = Patient::findOrFail($data['patient_id']);

        $treatment = Treatment::create($data);

        $treatment->save();

        $treatment->patient()->associate($patient);

        return $treatment;
    }

    public function show($id)
    {
        return Treatment::findOrFail($id);
    }

    public function update($data, $id)
    {
        $treatment = Treatment::findOrFail($id);

        if ($treatment->isClean()) {
            return response()->json([
                'message' => 'At least one value must change'
            ], Response::HTTP_UNPROCESSABLE_ENTITY );
        }

        $treatment->update($data);

        return $treatment;
    }

    public function delete($id)
    {
        $treatment = Treatment::findOrFail($id);

        $treatment->delete();

        return $treatment;
    }
}
