<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/2/19
 * Time: 9:00 PM
 */

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    protected $table = 'treatments';

    protected $fillable = [
        'patient_id',
        'user_id',
        'diagnosis',
        'illness',
        'recommendation',
        'date'
    ];

    protected $hidden = [ 'id' ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }
}
