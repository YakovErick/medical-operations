<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/5/19
 * Time: 10:03 AM
 */

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $table = 'referrals';

    protected $fillable = [
        'patient_id',
        'department_from',
        'department_to',
        'referred_by',
        'date'
    ];

    protected $guarded = [ 'id' ];

    public function practitioner()
    {
        return $this->belongsTo(User::class, 'referred_by');
    }

    public function departmentFrom()
    {
        return $this->belongsTo(Department::class, 'department_from');
    }

    public function departmentTo()
    {
        return $this->belongsTo(Department::class, 'department_to');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }
}
