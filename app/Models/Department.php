<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/2/19
 * Time: 8:10 PM
 */

namespace App\Models;


use App\User;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';

    protected $fillable = [ 'name', 'slug', 'can_refer' ];

    protected $guarded = [ 'id' ];

    public function users()
    {
        return $this->hasMany(User::class, 'department_id');
    }
}
