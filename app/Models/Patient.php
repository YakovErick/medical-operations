<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/2/19
 * Time: 9:00 PM
 */

namespace App\Models;


use App\Traits\Encryptable;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Patient extends Model
{
    use Encryptable, SearchableTrait;

    protected $table = 'patients';

    protected $fillable = ['name'];

    protected $encryptable = [
        'sex',
        'blood_type',
        'phone',
        'email',
        'emergency_contact'
    ];

    protected $searchable = [
        'columns' => [
            'name' => 10
        ],
    ];

    protected $guarded = [ 'id' ];

    public function checkins()
    {
        return $this->hasMany(Checkin::class, 'patient_id');
    }
}
