<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/5/19
 * Time: 10:03 AM
 */

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Checkin extends Model
{
    protected $table = 'checkins';

    protected $fillable = [
        'patient_id',
        'department_id',
        'time_in',
        'time_out',
        'checked',
        'checked_in_by'
    ];

    protected $guarded = [ 'id' ];

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id');
    }

    public function practitioner()
    {
        return $this->belongsTo(User::class, 'checked_in_by');
    }
}
