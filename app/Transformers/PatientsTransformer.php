<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 6:08 AM
 */

namespace App\Transformers;


use App\Models\Patient;
use Carbon\Carbon;

class PatientsTransformer
{
    public function transform(Patient $patient)
    {
        $treatment = $patient->treatments->last();

        $user = $treatment->user;

        return [
            'name' => $patient->name,
            'department' => $user->department->name,
            'user' => $user->name,
            'date' => Carbon::parse($treatment->date)->toFormattedDateString(),
            'illnes' => $treatment->illness,
            'id' => $treatment->id,
        ];
    }
}
