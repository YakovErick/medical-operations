<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 6:08 AM
 */

namespace App\Transformers;


use App\Models\Treatment;
use Carbon\Carbon;

class TreatmentsTransformer
{
    public function transform(Treatment $treatment)
    {
        $user = $treatment->user;

        return [
            'name' => $treatment->patient->name,
            'department' => $user->department->name,
            'user' => $user->name,
            'date' => Carbon::parse($treatment->date)->toFormattedDateString(),
        ];
    }
}
