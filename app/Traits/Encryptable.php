<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/2/19
 * Time: 9:14 PM
 */

namespace App\Traits;


use Illuminate\Support\Facades\Crypt;

trait Encryptable
{
    /**
     * Decryption for patients attributes
     * @param $key
     */
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (in_array($key, $this->encryptable)) {
            $value = Crypt::decrypt($value);
        }

        return $value;
    }

    /**
     * Encryption for patients attributes
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable)) {
            $value = Crypt::encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }
}
