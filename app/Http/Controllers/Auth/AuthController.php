<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 11:39 AM
 */

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public $successStatus = 200;

    public function login(){

        if(Auth::attempt([ 'email' => request('email'), 'password' => request('password') ])){

            $user = Auth::user();

            $success['token'] =  $user->createToken('MyApp')->accessToken;

            $user->token = $success['token'];

            return view('home', [ 'user' => $user ]);

        } else {

            return Redirect::back();
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();

        $input['password'] = bcrypt($input['password']);
        $input['department_id'] = rand(1, 6);

        $user = User::create($input);

        $success['token'] =  $user->createToken('MyApp')-> accessToken;

        $success['name'] =  $user->name;

        $user->token = $success['token'];

        return view('home', [ 'user' => $user ]);
    }
}
