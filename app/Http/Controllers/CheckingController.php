<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/5/19
 * Time: 10:35 AM
 */

namespace App\Http\Controllers;


use App\Repositories\CheckinRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckingController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new CheckinRepository();
    }

    public function store(Request $request)
    {
        $checkin = $this->repo->save($request->all());

        return response()
            ->json([
                'data' => $checkin
            ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $checkin = $this->repo->show($id);

        return response()->json($checkin);
    }

    public function update(Request $request, $id)
    {
        $checkin = $this->repo->update($request->all(), $id);

        return response()->json($checkin);
    }

    public function destroy($id)
    {
        $checkin = $this->repo->delete($id);

        return response()->json($checkin);
    }
}
