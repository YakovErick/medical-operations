<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 5:55 AM
 */

namespace App\Http\Controllers;


use App\Repositories\TreatmentRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TreatmentController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new TreatmentRepository();
    }

    public function index()
    {
        $patients = $this->repo->fetchTreatments(request()->all());

        return response()->json($patients);
    }

    public function store(Request $request)
    {
        $treatment = $this->repo->save($request->all());

        return response()
            ->json([
                'data' => $treatment
            ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $treatment = $this->repo->show($id);

        return response()->json($treatment);
    }

    public function update(Request $request, $id)
    {
        $treatment = $this->repo->update($request->all(), $id);

        return response()->json($treatment);
    }

    public function destroy($id)
    {
        $treatment = $this->repo->delete($id);

        return response()->json($treatment);
    }
}
