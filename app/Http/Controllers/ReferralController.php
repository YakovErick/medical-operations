<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/5/19
 * Time: 10:43 AM
 */

namespace App\Http\Controllers;


use App\Repositories\ReferralRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReferralController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new ReferralRepository();
    }

    public function store(Request $request)
    {
        $referral = $this->repo->save($request->all());

        return response()
            ->json([
                'data' => $referral
            ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $referral = $this->repo->show($id);

        return response()->json($referral);
    }

    public function update(Request $request, $id)
    {
        $referral = $this->repo->update($request->all(), $id);

        return response()->json($referral);
    }

    public function destroy($id)
    {
        $referral = $this->repo->delete($id);

        return response()->json($referral);
    }
}
