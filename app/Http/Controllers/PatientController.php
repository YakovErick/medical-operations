<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 5/3/19
 * Time: 5:55 AM
 */

namespace App\Http\Controllers;


use App\Repositories\PatientRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PatientController extends Controller
{
    protected $repo;

    public function __construct()
    {
        $this->repo = new PatientRepository();
    }

    public function index()
    {
        $patients = $this->repo->fetchPatients();

        return response()->json($patients);
    }

    public function store(Request $request)
    {
        $patient = $this->repo->save($request->all());

        return response()
            ->json([
                'data' => $patient
            ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $patient = $this->repo->show($id);

        return response()->json($patient);
    }

    public function update(Request $request, $id)
    {
        $patient = $this->repo->update($request->all(), $id);

        return response()->json($patient);
    }

    public function destroy($id)
    {
        $patient = $this->repo->delete($id);

        return response()->json($patient);
    }
}
