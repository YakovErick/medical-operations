import axios from 'axios';

export const fetchTreatments = function (form) {
    return axios.post('/api/treatments'+form.page, form.filters);
};

export const fetchDepartments = function () {
    return axios.get('/api/departments');
};
