import { mapGetters } from 'vuex';

const TreatmentController = {

    data() {
        return {
            department_id: null,
            name: null
        }
    },

    computed: {
        ...mapGetters({
            treatments: 'treatments',
            departments: 'departments',
            filters: 'filters',
            page_state: 'page_state'
        })
    },

    watch: {
        token: {
            handler: 'setToken',
            immediate: true
        },

        department_id: {
            handler: 'setDepartment',
            immediate: false
        },

        name: {
            handler: 'setPatient',
            immediate: false
        }
    },

    methods: {

        setDepartment() {
            this.filters.department_id = this.department_id;
            this.name = null;

            this.fetchTreatments();
        },

        setPatient() {
            this.department_id = null;
            this.filters.name = this.name;

            this.fetchTreatments();
        },

        setToken() {
            this.$store.commit('SET_DEFAULT_HEADER', this.token.token)
        },

        fetchTreatments() {
            this.$store.commit('FETCH_TREATMENTS');
        },

        fetchDepartments() {
            this.$store.commit('FETCH_DEPARTMENTS');
        },

        movePage(value) {
            this.$store.commit('FETCH_TREATMENTS', {
                prev: value === 'prev',
                next: value === 'next'
            })
        },

        nextPage() {

        }
    },

    mounted() {
        this.fetchTreatments();
        this.fetchDepartments();
    }
};

export default TreatmentController;
