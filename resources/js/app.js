require('./bootstrap');

window.Vue = require('vue');

import router from './router';
import store from './store/index';

Vue.component('home-component', require('./components/index').default);

const app = new Vue({
    el: '#app',
    router,
    store
});
