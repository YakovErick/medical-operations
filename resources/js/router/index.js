import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from '../components/index';

const router = new VueRouter({
    mode: 'history',

    routes : [
        {
            name: 'home',
            path: '/',
            component: Home
        }
    ]
});

export default router;

router.beforeEach((to, from, next) => {
    next();
});
