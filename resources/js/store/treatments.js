import * as indexApi from '../apis/indexApi';
import { Message } from 'element-ui';

const state = {
    treatments: [],
    treatments_paginate: [],
    filters: {},
    page_state: ''
};

const getters = {
    treatments: state => state.treatments,
    treatments_paginate: state => state.treatments_paginate,
    page_state: state => state.page_state,
    filters: state => state.filters,
};

const mutations = {

    FETCH_TREATMENTS(state, options) {

        state.fetchingTreatments = true;

        let form = {
            filters: state.filters,
            page: setNextPage(state, options)
        };

        indexApi.fetchTreatments(form)
            .then( ({ data }) => {
                state.treatments_paginate = data;
                state.treatments = data.data;

                state.page_state = 'Page ' + data.current_page + ' of ' + data.last_page;

                state.fetchingTreatments = false;
            }, () => {
                state.fetchingTreatments = true;
            });
    }
};

function setNextPage(state, options) {

    let paginate = state.treatments_paginate;

    if(options && options.prev) {
        if(paginate.current_page === 1) {
            Message.warning({ message: 'We are on the first page!'});
        }
        return paginate.prev_page_url ? paginate.prev_page_url : '' ;
    }

    if(options && options.next) {
        if(paginate.current_page === paginate.last_page) {
            Message.warning({ message: 'We are on the last page!'});
        }
        return paginate.next_page_url ? paginate.next_page_url : '' ;
    }

    return '';
}

export default {
    state, getters, mutations
}
