import * as indexApi from '../apis/indexApi';

const state = {
    departments: []
};

const getters = {
    departments: state => state.departments,
};

const mutations = {

    FETCH_DEPARTMENTS(state) {

        state.fetchingDepartments = true;

        indexApi.fetchDepartments()
            .then( ({ data }) => {
                state.departments = data;

                state.fetchingDepartments = false;

            }, () => {
                state.fetchingDepartments = true;
            });
    }
};

export default {
    state, getters, mutations
}
