import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import Treatments from './treatments';
import Departments from './departments';

const mutations = {
    SET_DEFAULT_HEADER(state, token) {

        if(token) {
            localStorage.setItem('token', token);
        }

        let auth_token = localStorage.getItem('token');

        window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + auth_token;

        getTreatments();
    }
};

function getTreatments() {
    store.commit('FETCH_TREATMENTS')
}

const store = new Vuex.Store({
    mutations,
    modules: {
        Treatments,
        Departments
    }
});


export default store;
